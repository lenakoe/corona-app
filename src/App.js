import React, { Component } from 'react';
import Loader from 'react-loader'
import './App.scss';
import Header from './Components/Header/Header'
import DataColumn from './Components/DataColumn/DataColumn';
import Footer from './Components/Footer/Footer';
import logo from './Img/coronavirus.svg'
import cross from './Img/cross.svg';
import newBadge from './Img/new.svg';
import people from './Img/people.svg'
import getData from './Components/Proxy/Proxy';


class App extends Component {
  state = {
    areas: [],
    currentArea: {}, //areaData
    lastModified: '',
    loaded: false
  }
  
  componentDidMount() {
    getData()
    .then(response => {
      this.setState({ 
        lastModified: response.headers.get('Date').toString() 
      })
      return response.json()
    })
    .then(data => { 
      this.setState({ 
        areas: data.states, 
        currentArea: data.states[0], 
        loaded: true })
      })
      .catch(console.log('Es ist ein Fehler aufgetreten.'))
  }
  

  handleChange = (event) => {
    const currentIndex = [...event.target.children].filter(child => child.value === event.target.value)[0].index
    this.setState({ 
      currentArea: this.state.areas[currentIndex] 
    });
  }

  render() {
    const areas = this.state.areas;

    return (
      <div className="app">
        <Header 
          imgSrc={logo}
          headerHeading="Corona Fallzahlen" />
        <Loader loaded={this.state.loaded}>
          <div className="container">
            <p className="container__text--introduction">Wähle ein Bundesland aus, um die zugehörigen Fallzahlen zu sehen.</p>
            <form className="form">
              <label 
                className="form__label" 
                htmlFor="options">Bundesland</label>
              <select 
                className="form__selection"
                id="options" 
                onChange={this.handleChange} >
                {areas.map((area) => (                
                  <option 
                    value={area.name.toLowerCase().replace(/\u00fc/, 'ue').replace(/(\shy;|­| #173;)/, "")} 
                    key={area.code} >
                    {area.name}
                  </option>
              ))}
              </select>
            </form>
              <h2 className="container__heading">{this.state.currentArea.name}</h2>
              <DataColumn 
                imgSrc={people}
                alt="Platzhalterbild"
                dataHeading="Gesamt"
                number={this.state.currentArea.count} />
              <DataColumn 
                imgSrc={newBadge}
                alt="Platzhalterbild"
                dataHeading="Vergleich zum Vortag"
                number={'+'+this.state.currentArea.difference} />
              <DataColumn 
                imgSrc={cross}
                alt="Platzhalterbild"
                dataHeading="Verstorben"
                number={this.state.currentArea.deaths} />
          </div>
          <Footer
            date={this.state.lastModified}/>
        </Loader>
      </div>
      );
    }
  }


export default App;