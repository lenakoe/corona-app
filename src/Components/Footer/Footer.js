import React from 'react';

const footer = (props) => {

    return (
    <footer className="footer">
        <p className="footer__text">Daten laut RKI. Die letzte Datenabfrage erfolgte am {props.date}</p>
    </footer>
    )
}

export default footer;