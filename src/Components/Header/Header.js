import React from 'react';

const header = (props) => {
    return(
        <header className="header">
            <img className="header__logo" src={props.imgSrc} alt="logo"></img>
            <h1 className="header__heading">{props.headerHeading}</h1>
        </header>
    )
}

export default header