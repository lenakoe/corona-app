import React from 'react';

const dataColumn = (props) => {
    return(
        <div className="column">
            <div className="column__image--wrapper">
                <img className="column__image" src={props.imgSrc} alt={props.altText}/>
            </div>
            <h3 className="column__text--heading">{props.dataHeading}</h3>
            <p className="column__text--number">{props.number}</p>
        </div>
    )
}

export default dataColumn;